﻿#define _CRT_SECURE_NO_WARNINGS // эта деректива уберегает от ошибки, связанной с логикой получения текущей даты
#include <iostream>
#include <ctime>
using namespace std;

int main()
{
    time_t now = time(0);
    tm* ltm = localtime(&now);
    int day = ltm->tm_mday;
    cout << "Current Day of the Month: " << day << endl;
    const int size1 = 6;
    const int size2 = 6;
    int array[size1][size2];
    int sumForString;
    int indexOfArray = day % size1;
    cout << "Index Of Array = " << indexOfArray << endl;
    int total = 0;

    for (int i = 0; i < size1; i++)
    {
        int sum = 0;
        for (int j = 0; j < size2; j++)
        {
            array[i][j] = i + j;
            sum += array[i][j];
            total += array[i][j];
            cout << array[i][j] << " ";
        }
        sumForString = sum;

        if (i == indexOfArray)
        {
            cout << "Sum of array: " << sumForString;
        }
        cout << '\n';
        
    }
    cout << "Total Sum of arrays: " << total << endl;

    return 0;
}
